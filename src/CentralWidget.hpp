#pragma once

#include <QtWidgets>
#include "src/native_shortcuts/NativeShortcutEdit.hpp"
#include "Options.hpp"

class CentralWidget : public QWidget
{
	Q_OBJECT

	private:
		QPushButton* m_applyOptionsButton;
		NativeShortcutEdit* m_shortcutEdit;

		Options m_options;

	public:
		CentralWidget(QWidget* parent);

		void loadOptions(const Options& options);
		Options options() const {return m_options;}

	private:
		void optionsChanged();

	private slots:
		void applyOptions();

	signals:
		void applyRequested();
		void hideRequested();
};

