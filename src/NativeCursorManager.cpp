#include "NativeCursorManager.hpp"

bool NativeCursorManager::m_isVisible = true;

void NativeCursorManager::toggleVisible()
{
	setVisible(!m_isVisible);
}

void NativeCursorManager::show()
{
	setVisible(true);
}

void NativeCursorManager::setVisible(bool visible)
{
	if (visible == m_isVisible)
	{
		return;
	}

	static const CursorIdVector cursorIds({
		OCR_APPSTARTING,
		OCR_CROSS,
		OCR_HAND,
		OCR_IBEAM,
		OCR_ICOCUR,
		OCR_ICON,
		OCR_NO,
		OCR_NORMAL,
		OCR_SIZE,
		OCR_SIZEALL,
		OCR_SIZENESW,
		OCR_SIZENS,
		OCR_SIZENWSE,
		OCR_SIZEWE,
		OCR_UP,
		OCR_WAIT
	});

	static CursorMap defaultCursors;
	if (defaultCursors.isEmpty())
	{
		for (CursorId cursorId : cursorIds)
		{
			LPSTR cursorName = MAKEINTRESOURCE(cursorId);
			Cursor cursor = (Cursor)LoadImageA(nullptr, cursorName, IMAGE_CURSOR, 0, 0, LR_SHARED);
			Cursor cursorCopy = (Cursor) CopyImage(cursor, IMAGE_CURSOR, 0, 0, 0);
			defaultCursors.insert(cursorId, cursorCopy);
		}
	}

	static Cursor transparentCursor = createTransparentCursor();

	for (CursorId cursorId : cursorIds)
	{
		Cursor cursor =
			visible
			? defaultCursors.value(cursorId)
			: transparentCursor;

		Cursor cursorCopy = (Cursor) CopyImage(cursor, IMAGE_CURSOR, 0, 0, 0);
		SetSystemCursor(cursorCopy, cursorId);
	}

	m_isVisible = visible;
}

NativeCursorManager::Cursor NativeCursorManager::createTransparentCursor()
{
	static const int cursorSize = 32;

	static quint8 andMask[cursorSize * 4];
	for (int i = 0; i < cursorSize * 4; ++i)
	{
		andMask[i] = 0xFF;
	}

	static quint8 orMask[cursorSize * 4];
	for (int i = 0; i < cursorSize * 4; ++i)
	{
		orMask[i] = 0x00;
	}

	return CreateCursor(nullptr, 0, 0, cursorSize, cursorSize, &andMask, &orMask);
}

