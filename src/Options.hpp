#pragma once

#include "src/native_shortcuts/NativeShortcut.hpp"

class Options
{
	private:
		NativeShortcut m_toggleCursorShortcut;

		static QString toggleCursorShortcutKey() {return "toggleCursorShortcut";}

	public:
		NativeShortcut toggleCursorShortcut() const {return m_toggleCursorShortcut;}
		void setToggleCursorShortcut(const NativeShortcut& toggleCursorShortcut) {m_toggleCursorShortcut = toggleCursorShortcut;}

		void saveToQSettings(QSettings* settings) const;
		void loadFromQSettings(QSettings* settings);
};

