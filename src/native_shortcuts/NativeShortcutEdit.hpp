#pragma once

#include <QtWidgets>
#include "NativeShortcut.hpp"
#include "NativeShortcutDialog.hpp"

class NativeShortcutEdit : public QWidget
{
	Q_OBJECT

	private:
		NativeShortcut m_selectedShortcut;
		QLineEdit* m_lineEdit;
		QPushButton* m_clearButton;
		NativeShortcutDialog* m_shortcutDialog;

	public:
		NativeShortcutEdit(QWidget* parent);
		NativeShortcut selectedShortcut() const {return m_selectedShortcut;}
		void setSelectedShortcut(NativeShortcut shortcut);

	private slots:
		void onDialogAccepted();
		void onEditShortcut();
		void onClearShortcut();

	signals:
		void shortcutChanged();
};

