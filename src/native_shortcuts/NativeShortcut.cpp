#include "NativeShortcut.hpp"

NativeShortcut::NativeShortcut(QKeyEvent* event)
	: m_isNull(false)
	, m_virtualKey(event->nativeVirtualKey())
	, m_modifiers(0)
{
	const Qt::KeyboardModifiers qtModifiers = event->modifiers();

	if (qtModifiers & Qt::ControlModifier)
	{
		m_string += "Ctrl+";
		m_modifiers |= MOD_CONTROL;
	}

	if (qtModifiers & Qt::ShiftModifier)
	{
		m_string += "Shift+";
		m_modifiers |= MOD_SHIFT;
	}

	if (qtModifiers & Qt::AltModifier)
	{
		m_string += "Alt+";
		m_modifiers |= MOD_ALT;
	}

	if (qtModifiers & Qt::MetaModifier)
	{
		m_string += "Win+";
		m_modifiers |= MOD_WIN;
	}

	m_string += QKeySequence(event->key()).toString();
}

NativeShortcut::NativeShortcut()
	: m_isNull(true)
{
}

bool NativeShortcut::operator==(const NativeShortcut& other) const
{
	return m_isNull == other.m_isNull
		   && m_virtualKey == other.m_virtualKey
		   && m_modifiers == other.m_modifiers;
}

bool NativeShortcut::operator!=(const NativeShortcut& other) const
{
	return !operator==(other);
}

QVariant NativeShortcut::toVariant() const
{
	QVariantMap result;
	result.insert(isNullKey(), m_isNull);
	result.insert(stringKey(), m_string);
	result.insert(virtualKeyKey(), (qulonglong)m_virtualKey);
	result.insert(modifiersKey(), (qulonglong)m_modifiers);
	return result;
}

NativeShortcut NativeShortcut::fromVariant(const QVariant& variant)
{
	const QVariantMap map = variant.toMap();
	NativeShortcut result;
	result.m_isNull = map.value(isNullKey()).toBool();
	result.m_string = map.value(stringKey()).toString();
	result.m_virtualKey = map.value(virtualKeyKey()).toULongLong();
	result.m_modifiers = map.value(modifiersKey()).toULongLong();
	return result;
}

