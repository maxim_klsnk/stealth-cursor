#pragma once

#include <QtWidgets>
#include "NativeShortcut.hpp"

class NativeShortcutDialog : public QDialog
{
	Q_OBJECT

	private:
		QLineEdit* m_shortcutEdit;
		NativeShortcut m_selectedShortcut;

	public:
		NativeShortcutDialog(QWidget* parent);
		NativeShortcut selectedShortcut() const {return m_selectedShortcut;}

	protected:
		void keyReleaseEvent(QKeyEvent* event) override;

	public slots:
		void open() override;
};

