#include "NativeShortcutDialog.hpp"

NativeShortcutDialog::NativeShortcutDialog(QWidget* parent)
	: QDialog(parent)
{
	setWindowTitle("Shortcut");
	setWindowFlag(Qt::WindowContextHelpButtonHint, false);

	m_shortcutEdit = new QLineEdit();
	m_shortcutEdit->setReadOnly(true);
	m_shortcutEdit->setText("Press shortcut...");

	QDialogButtonBox* buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok);
	connect(
		buttonBox, &QDialogButtonBox::accepted,
		this, &QDialog::accept);


	QVBoxLayout* layout = new QVBoxLayout();
	layout->addWidget(m_shortcutEdit);
	layout->addStretch(1000);
	layout->addWidget(buttonBox);

	setLayout(layout);
}

void NativeShortcutDialog::keyReleaseEvent(QKeyEvent* event)
{
	if (event->key() != Qt::Key_Control
		&& event->key() != Qt::Key_Shift
		&& event->key() != Qt::Key_Alt
		&& event->key() != Qt::Key_Meta)
	{
		m_selectedShortcut = NativeShortcut(event);
		m_shortcutEdit->setText(m_selectedShortcut.toString());
	}

	QWidget::keyReleaseEvent(event);
}

void NativeShortcutDialog::open()
{
	m_shortcutEdit->setText("Press shortcut...");
	m_selectedShortcut = NativeShortcut();
	QDialog::open();
}
