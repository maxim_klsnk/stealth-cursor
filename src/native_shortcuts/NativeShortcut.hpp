#pragma once

#include <QtWidgets>

class NativeShortcut
{
	private:
		bool m_isNull;
		QString m_string;
		quint32 m_virtualKey;
		quint32 m_modifiers;

		static QString isNullKey() {return "isNull";}
		static QString stringKey() {return "string";}
		static QString virtualKeyKey() {return "virtualKey";}
		static QString modifiersKey() {return "modifiers";}

	public:
		NativeShortcut();
		NativeShortcut(QKeyEvent* event);

		bool isNull() const {return m_isNull;}
		QString toString() const {return m_string;}
		QVariant toVariant() const;
		qint32 scanCode() const {return m_virtualKey;}
		qint32 modifiers() const {return m_modifiers;}
		bool operator==(const NativeShortcut& other) const;
		bool operator!=(const NativeShortcut& other) const;

		static NativeShortcut fromVariant(const QVariant& variant);
};

