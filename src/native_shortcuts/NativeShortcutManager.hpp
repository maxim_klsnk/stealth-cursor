#pragma once

#include <QtWidgets>
#include "NativeShortcut.hpp"
#include "NativeShortcutReceiverWindow.hpp"

class NativeShortcutManager : public QObject
{
	Q_OBJECT

	private:
		NativeShortcutReceiverWindow* m_receiverWindow;

	public:
		NativeShortcutManager(QObject* parent = nullptr);
		~NativeShortcutManager();

		bool add(const NativeShortcut& shortcut, int shortcutId);
		bool remove(int shortcutId);
		void removeAll();

	signals:
		void triggered(int shortcutId);
};

