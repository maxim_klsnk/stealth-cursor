#include "NativeShortcutReceiverWindow.hpp"

bool NativeShortcutReceiverWindow::nativeEvent(const QByteArray& /*eventType*/, void* message, long* /*result*/)
{
	MSG* nativeMessage = (MSG*)message;

	if (nativeMessage->message == WM_HOTKEY) {
		emit shortcutTriggered(nativeMessage->wParam);
		return true;
	}

	return false;
}
