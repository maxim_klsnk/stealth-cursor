#pragma once

#include <QtWidgets>

class NativeShortcutReceiverWindow : public QWindow
{
	Q_OBJECT

	protected:
		bool nativeEvent(const QByteArray &eventType, void *message, long *result) override;

	signals:
		void shortcutTriggered(int shortcutId);
};

