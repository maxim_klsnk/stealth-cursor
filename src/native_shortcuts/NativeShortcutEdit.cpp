#include "NativeShortcutEdit.hpp"

NativeShortcutEdit::NativeShortcutEdit(QWidget* parent)
	: QWidget(parent)
	, m_shortcutDialog(new NativeShortcutDialog(this))
{
	connect(
		m_shortcutDialog, &QDialog::accepted,
		this, &NativeShortcutEdit::onDialogAccepted);

	QPushButton* editButton = new QPushButton("Edit...");
	connect(
		editButton, &QAbstractButton::clicked,
		this, &NativeShortcutEdit::onEditShortcut);

	m_clearButton = new QPushButton("Clear");
	m_clearButton->setEnabled(false);
	connect(
		m_clearButton, &QAbstractButton::clicked,
		this, &NativeShortcutEdit::onClearShortcut);

	m_lineEdit = new QLineEdit();
	m_lineEdit->setReadOnly(true);
	m_lineEdit->setText("Empty");

	QHBoxLayout* layout = new QHBoxLayout();
	layout->addWidget(m_lineEdit, 2);
	layout->addWidget(m_clearButton, 1);
	layout->addWidget(editButton, 1);
	layout->setMargin(0);

	setLayout(layout);
}

void NativeShortcutEdit::setSelectedShortcut(NativeShortcut shortcut)
{
	if (shortcut == m_selectedShortcut)
	{
		return;
	}

	m_selectedShortcut = shortcut;
	m_clearButton->setEnabled(!shortcut.isNull());
	if (shortcut.isNull())
	{
		m_lineEdit->setText("Empty");
	}
	else
	{
		m_lineEdit->setText(shortcut.toString());
	}
}

void NativeShortcutEdit::onDialogAccepted()
{
	NativeShortcut shortcut = m_shortcutDialog->selectedShortcut();
	setSelectedShortcut(shortcut);
	emit shortcutChanged();
}

void NativeShortcutEdit::onEditShortcut()
{
	m_shortcutDialog->open();
}

void NativeShortcutEdit::onClearShortcut()
{
	setSelectedShortcut(NativeShortcut());
	emit shortcutChanged();
}
