#include "NativeShortcutManager.hpp"
#include <windows.h>

NativeShortcutManager::NativeShortcutManager(QObject* parent)
	: QObject(parent)
{
	m_receiverWindow = new NativeShortcutReceiverWindow();

	connect(
		m_receiverWindow, &NativeShortcutReceiverWindow::shortcutTriggered,
		this, &NativeShortcutManager::triggered);
}

NativeShortcutManager::~NativeShortcutManager()
{
	m_receiverWindow->deleteLater();
}

void NativeShortcutManager::removeAll()
{
	m_receiverWindow->deleteLater();
	m_receiverWindow = new NativeShortcutReceiverWindow();

	connect(
		m_receiverWindow, &NativeShortcutReceiverWindow::shortcutTriggered,
		this, &NativeShortcutManager::triggered);
}

bool NativeShortcutManager::add(const NativeShortcut& shortcut, int shortcutId)
{
	return RegisterHotKey(
			(HWND)m_receiverWindow->winId(),
			shortcutId,
			shortcut.modifiers(),
			shortcut.scanCode());
 }

bool NativeShortcutManager::remove(int shortcutId)
{
	return UnregisterHotKey(
			(HWND)m_receiverWindow->winId(),
			shortcutId);
}
