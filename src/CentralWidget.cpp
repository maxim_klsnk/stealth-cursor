#include "CentralWidget.hpp"

CentralWidget::CentralWidget(QWidget* parent)
	: QWidget(parent)
{
	QFormLayout* optionsFormLayout = new QFormLayout();


	m_shortcutEdit = new NativeShortcutEdit(this);
	connect(
			m_shortcutEdit, &NativeShortcutEdit::shortcutChanged,
			this, &CentralWidget::optionsChanged);
	optionsFormLayout->addRow("Toggle cursor shortcut:", m_shortcutEdit);

	QHBoxLayout* optionsLayout = new QHBoxLayout();
	optionsLayout->addLayout(optionsFormLayout);
	optionsLayout->addStretch(1000);

	QGroupBox* optionsGroupBox = new QGroupBox("Options");
	optionsGroupBox->setLayout(optionsLayout);

	QHBoxLayout* bottomButtonsLayout = new QHBoxLayout();
	bottomButtonsLayout->addStretch();
	m_applyOptionsButton = new QPushButton("Apply");
	connect(
			m_applyOptionsButton, &QAbstractButton::clicked,
			this, &CentralWidget::applyOptions);
	m_applyOptionsButton->setEnabled(false);
	bottomButtonsLayout->addWidget(m_applyOptionsButton);
	QPushButton* hideButton = new QPushButton("Hide");
	connect(
		hideButton, &QAbstractButton::clicked,
		this, &CentralWidget::hideRequested);
	bottomButtonsLayout->addWidget(hideButton);

	QVBoxLayout* layout = new QVBoxLayout();
	layout->addWidget(optionsGroupBox);
	layout->addLayout(bottomButtonsLayout);
	setLayout(layout);
}

void CentralWidget::optionsChanged()
{
	m_applyOptionsButton->setEnabled(true);
	m_options.setToggleCursorShortcut(m_shortcutEdit->selectedShortcut());
}

void CentralWidget::applyOptions()
{
	m_applyOptionsButton->setEnabled(false);
	emit applyRequested();
}

void CentralWidget::loadOptions(const Options& options)
{
	m_options = options;
	m_shortcutEdit->setSelectedShortcut(options.toggleCursorShortcut());
}

