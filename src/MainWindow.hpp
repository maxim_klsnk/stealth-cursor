#pragma once

#include <QtWidgets>
#include "CentralWidget.hpp"
#include "native_shortcuts/NativeShortcutManager.hpp"

class MainWindow : public QMainWindow
{
	Q_OBJECT

	private:
		QSystemTrayIcon* m_trayIcon;
		CentralWidget* m_centralWidget;
		NativeShortcutManager* m_shortcutManager;

		static constexpr const int ToggleCursorShortcutId = 1;

	public:
		MainWindow();

	protected:
		void closeEvent(QCloseEvent* event) override;

	private:
		void saveOptions();
		void loadOptions();
		void applyOptions(const Options& options);
		void onShortcutTriggered(int shortcutId);

	private slots:
		void onTrayIconExit();
		void onTrayIconActivated(QSystemTrayIcon::ActivationReason activationReason);
};

