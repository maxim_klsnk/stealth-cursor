#pragma once

#define OEMRESOURCE

#include <QtCore>
#include <windows.h>

class NativeCursorManager
{
	private:
		typedef DWORD CursorId;
		typedef HCURSOR Cursor;
		typedef QVector<CursorId> CursorIdVector;
		typedef QMap<CursorId, Cursor> CursorMap;

		static bool m_isVisible;

		static void setVisible(bool visible);
		static Cursor createTransparentCursor();

	public:
		static bool isVisible() {return m_isVisible;};
		static void toggleVisible();
		static void show();
};

