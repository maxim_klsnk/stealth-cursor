#include "Options.hpp"

void Options::saveToQSettings(QSettings* settings) const
{
	settings->setValue(toggleCursorShortcutKey(), m_toggleCursorShortcut.toVariant());
}

void Options::loadFromQSettings(QSettings* settings)
{
	const QVariant toggleCursorShortcutVariant = settings->value(toggleCursorShortcutKey());

	m_toggleCursorShortcut =
		toggleCursorShortcutVariant.isValid()
		? NativeShortcut::fromVariant(toggleCursorShortcutVariant)
		: NativeShortcut();
}
