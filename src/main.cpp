#include <QtWidgets>
#include "MainWindow.hpp"

int main(int argc, char** argv)
{
    QApplication application(argc, argv);
	MainWindow* mainWindow = new MainWindow();
	mainWindow->show();
    return QApplication::exec();
}