#include "MainWindow.hpp"
#include "NativeCursorManager.hpp"

MainWindow::MainWindow()
{
	QCoreApplication::setOrganizationName("Klsnk");
	QCoreApplication::setOrganizationDomain("klsnk.com");
	QCoreApplication::setApplicationName("Stealth Cursor");

	const QIcon icon(":/icon.ico");

	m_centralWidget = new CentralWidget(this);
	connect(
		m_centralWidget, &CentralWidget::hideRequested,
		this, &QWidget::hide);
	connect(
		m_centralWidget, &CentralWidget::applyRequested,
		this, &MainWindow::saveOptions);

	setWindowIcon(icon);
	setWindowTitle("Stealth cursor");
	setCentralWidget(m_centralWidget);
	setWindowFlag(Qt::WindowMinMaxButtonsHint, false);

	QScreen *screen = QGuiApplication::primaryScreen();
	int physicalDPIX = (int)screen->physicalDotsPerInchX();
	int physicalDPIY = (int)screen->physicalDotsPerInchY();
	int proportionX = 5;
	int proportionY = 2;

	resize(QSize(physicalDPIX * proportionX, physicalDPIY * proportionY));

	QMenu* menu = new QMenu();
	menu->addAction("Options", this, &QWidget::show);
	menu->addAction("Exit", this, &MainWindow::onTrayIconExit);

	m_trayIcon = new QSystemTrayIcon(this);
	m_trayIcon->setContextMenu(menu);
	m_trayIcon->setIcon(icon);
	m_trayIcon->show();
	connect(
		m_trayIcon, &QSystemTrayIcon::activated,
		this, &MainWindow::onTrayIconActivated);

	m_shortcutManager = new NativeShortcutManager(this);
	connect(
		m_shortcutManager, &NativeShortcutManager::triggered,
		this, &MainWindow::onShortcutTriggered);

	loadOptions();
}

void MainWindow::onTrayIconActivated(QSystemTrayIcon::ActivationReason activationReason)
{
	if (activationReason == QSystemTrayIcon::Trigger)
	{
		show();
	}
}

void MainWindow::closeEvent(QCloseEvent* event)
{
	event->ignore();
	hide();
}

void MainWindow::onTrayIconExit()
{
	m_trayIcon->hide();
	NativeCursorManager::show();
	QCoreApplication::quit();
}

void MainWindow::saveOptions()
{
	const Options options = m_centralWidget->options();
	QSettings settings;
	options.saveToQSettings(&settings);
	settings.sync();
	applyOptions(options);
}

void MainWindow::loadOptions()
{
	QSettings settings;
	settings.sync();
	Options options;
	options.loadFromQSettings(&settings);
	m_centralWidget->loadOptions(options);
	applyOptions(options);
}

void MainWindow::applyOptions(const Options& options)
{
	NativeShortcut toggleCursorShortcut = options.toggleCursorShortcut();
	m_shortcutManager->remove(ToggleCursorShortcutId);
	if (!toggleCursorShortcut.isNull())
	{
		m_shortcutManager->add(toggleCursorShortcut, ToggleCursorShortcutId);
	}
}

void MainWindow::onShortcutTriggered(int shortcutId)
{
	if (shortcutId == ToggleCursorShortcutId)
	{
		NativeCursorManager::toggleVisible();
	}
}
